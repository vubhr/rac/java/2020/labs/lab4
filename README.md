# Lab4

Vaš klijent je brzorastuća farmaceutska grupa koja u svojem vlasništvu ima jednu manju farmaceutsku kompaniju. Ta manja farmaceutska kompanija ima poslovnice na 3 različite lokacije.
Klijent od vas traži da napravite inventuru u svakoj od poslovnica (te manje farmaceutske kompanije) na način da evidentirate već pripravljene lijekove, kao i materijale za izradu lijekova.
Evidencija se radi na način da ispišete u konzolu:
Ime_farm.grupe -> ime_manje_farmaceutske_kompanije -> ime_poslovnice -> lokacija_poslovnice
Lijek_ili_materijal: broj
Lijek_ili_materijal: broj

Primjeri lijekova:
- Voltaren
- Lupocet
- Dermazin
- Ibuprofen
- Rojazol
- Maxitrol
- Betrion
- ...

Primjeri materijala:
- Voda
- Prašak
- Aloa vera
- Salicilna kiselina
- Parafin
- ...

Rok za izradu labosa je petak 27.3.2020. do 23:59.

**UPDATE:**

Klijent je postao vlasnik još jedne farmaceutske kompanije i želi da napravite inventuru i za tu novu kompaniju. Nova kompanija ima dvije poslovnice.

Klijent također želi da osim popisa lijekova, i materijala za izradu lijekova, popišete i pakiranja zaštitnih rukavica (pakiranja od 30 i 50 komada) koje postoje u dvije različite boje, bijele i crne, te pakiranja zaštitnih maska za lice (pakiranja od 30 i 50 komada).

S obzirom na nove zahtjeve klijenta, ispregovaran je novi rok za predaju rješenja. 

Implementaciju je potrebno **predati do ponedjeljka 30.3.2020. do 23:59.**